/**
 * 
 */
package application.dao;



/**
 * @author revanth
 *
 */
public class IncidentDetection {
	
	public String camera;
	public String startDate;
	public String endDate;
	
	public IncidentDetection(String camera, String startDate, String endDate) {
		super();
		this.camera = camera;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	
}
