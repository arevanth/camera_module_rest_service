/**
 * 
 */
package application.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import application.dao.IncidentDetection;
import application.sql.Queries;

/**
 * @author revanth
 *
 */

@Controller
public class ServiceController {
	
	@RequestMapping(value="/test", method=RequestMethod.GET)
	@ResponseBody
	public String testService()
	{
		return "The service is running.";
		
	}
	
	@RequestMapping(value="/incidents", method=RequestMethod.GET)
	@ResponseBody
	public List<IncidentDetection> getIncidents(@RequestParam("start") String startDate, @RequestParam(value="end",required=false) String endDate)
	{
		System.out.println(startDate);
		
		Queries q = new Queries();
		if(endDate != null)
			return q.getIncidentsBetweenTwoDates(startDate, endDate);
		else
			return q.getIncidentsAfterDate(startDate);
	}
}
