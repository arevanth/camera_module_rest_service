/**
 * 
 */
package application.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import application.dao.IncidentDetection;
import application.utils.Constants;

/**
 * @author revanth
 *
 */
public class Queries {
	
	public List<IncidentDetection> getIncidentsBetweenTwoDates(String StartDate, String EndDate)
	{
		List<IncidentDetection> incidents = new ArrayList<IncidentDetection>();
		
		try
		{
			Class.forName(Constants.DB_Driver);
			Connection conn = DriverManager.getConnection(Constants.DB_Connection,Constants.DB_User, Constants.DB_Password); 
			String query = "SELECT camera,start_date,end_date FROM detected_congestion WHERE start_date >= '" + StartDate + "' AND start_date <= '" + EndDate + "'";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next())
			{
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Timestamp startTimeStamp = rs.getTimestamp(2);
				Timestamp endTimeStamp = rs.getTimestamp(3);
				IncidentDetection incident = new IncidentDetection(rs.getString(1),dateFormat.format(startTimeStamp.getTime()),dateFormat.format(endTimeStamp.getTime()));
				incidents.add(incident);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return incidents;
	}
	
	public List<IncidentDetection> getIncidentsAfterDate(String date)
	{
		List<IncidentDetection> incidents = new ArrayList<IncidentDetection>();
		
		try
		{
			Class.forName(Constants.DB_Driver);
			Connection conn = DriverManager.getConnection(Constants.DB_Connection,Constants.DB_User, Constants.DB_Password); 
			String query = "SELECT camera,start_date,end_date FROM detected_congestion WHERE start_date >= '" + date + "'";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next())
			{
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Timestamp startTimeStamp = rs.getTimestamp(2);
				Timestamp endTimeStamp = rs.getTimestamp(3);
				IncidentDetection incident = new IncidentDetection(rs.getString(1),dateFormat.format(startTimeStamp.getTime()),dateFormat.format(endTimeStamp.getTime()));
				incidents.add(incident);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return incidents;
	}
}
